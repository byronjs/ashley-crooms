<?php
/* Template Name: Home Page */

get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<div class="section home-slide" data-anchor="firstPage">
				<div class="row">	
					<div class="col grid-12">	
						<img class="logo" src="http://ashleycrooms.com/wp-content/uploads/2014/02/ashley-crooms-logo.png">
					</div>	
					<div class="clear"></div>
				</div>
			</div>
			<div class="section about-slide" data-anchor="aboutPage">
				<div class="row">	
					<div class="col grid-12">
						<h1>ABOUT</h1>
						<div class="line"></div>
					</div>
				</div>
				<div class="row">	
					<div class="col grid-6">
						<?php the_field('about'); ?>
					</div>
					<div class="col grid-6">
						<?php the_field('about_b'); ?>
					</div>
				</div>
			</div>
			<div class="section services-slide" data-anchor="servicesPage">
				<div class="row">	
					<div class="col grid-12">
						<h1>SERVICES</h1>
						<div class="line"></div>
					</div>
				</div>
				<div class="row">
					<div class="col grid-2">
						<div class="price-container">
							<div class="price">	
								<span class="pnumber">$<?php the_field('service_1_price'); ?></span><br/>
								<span class="per-hour">PER HOUR </span>
							</div>
						</div>
					</div>
					<div class="col grid-5">
						<?php the_field('service_1_list_a'); ?>
					</div>
					<div class="col grid-5">
						<?php the_field('service_1_list_b'); ?>
					</div>
				</div>
				<div class="row">
					<div class="price-line"></div>
				</div>
				<div class="row">
					<div class="col grid-2">
						<div class="price-container">
							<div class="price">	
								<span class="pnumber">$<?php the_field('service_1_price'); ?></span><br/>
								<span class="per-hour">PER HOUR </span>
							</div>
						</div>
					</div>
					<div class="col grid-5">
						<?php the_field('service_1_list_a'); ?>
					</div>
					<div class="col grid-5">
						<?php the_field('service_1_list_b'); ?>
					</div>
					<div class="clear"></div>
				</div>

			</div>
			<div class="section contact-slide" data-anchor="contactPage">
				<div class="row">	
					<div class="col grid-12">
						<h1>CONTACT</h1>
						<div class="line"></div>
					</div>
				</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php endwhile; // End the loop ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>