<?php
/* Template Name: Home Page */

get_header(); ?>
<?php while (have_posts()) : the_post(); ?>
	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<div class="section home-slide" data-anchor="firstPage">
				<div class="row">	
					<div class="col grid-12">	
						<img class="logo" src="http://ashleycrooms.com/wp-content/uploads/2014/02/ashley-crooms-logo.png">
					</div>	
					<div class="clear"></div>
				</div>
			</div>
			<div class="section about-slide" data-anchor="aboutPage">
				<div class="row">	
					<div class="col grid-12">
						<h1>ABOUT</h1>
						<div class="line"></div>
					</div>
				</div>
				<div class="row">	
					<div class="col grid-6">
						<?php the_field('about'); ?>
					</div>
					<div class="col grid-6">
						<?php the_field('about_b'); ?>
					</div>
				</div>
			</div>
			<div class="section services-slide" data-anchor="servicesPage">
				<div class="row">	
					<div class="col grid-12">
						<h1>SERVICES</h1>
						<div class="line"></div>
					</div>
				</div>
				<div class="row service-lists">
					<div class="col grid-6 services-border">
						<h2><?php the_field('service_1_price'); ?></h2>
						<?php the_field('service_1_list'); ?>
					</div>
					<div class="col grid-6">
						<h2><?php the_field('service_2_price'); ?></h2>
						<?php the_field('service_2_list'); ?>
					</div>
				</div>
				<?php 

				$images = get_field('service_gallery');

				if( $images ): ?>
					<div class="image-gallery units-container">
					    <ul class="units-row" >
					        <?php foreach( $images as $image ): ?>
					            <li class="unit-25">
					                <a href="<?php echo $image['sizes']['large']; ?>">
					                     <img src="<?php echo $image['sizes']['gallery-thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
					                </a>
					                <p><?php echo $image['caption']; ?></p>
					            </li>
					        <?php endforeach; ?>
					    </ul>
				    </div>
			<?php endif; ?>

			</div>
			<div class="section contact-slide" data-anchor="contactPage">
				<div class="row">	
					<div class="col grid-12">
						<h1>CONTACT</h1>
						<div class="line"></div>
						<p class="contact-info">Currently taking new clients.</p>
						<?php echo do_shortcode("[contact-form-7 id='16' title='Contact form 1']"); ?>
					</div>
				</div>
			</div>

		</div><!-- #content -->
	</div><!-- #primary -->
<?php endwhile; // End the loop ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>