<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that other
 * 'pages' on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">

			<div class="flexslider">
				<ul class="slides">
						<?php 

								// check if the repeater field has rows of data
							if( have_rows('banners') ):

							 	// loop through the rows of data
							    while ( have_rows('banners') ) : the_row();

								$bannerImage = get_sub_field('image');
								$banner = get_sub_field( 'image' );
								$bannerSize = 'banner-image';
								$bannerImage = $banner['sizes'][$bannerSize];

						?>
							<?php if( get_sub_field('image') ) { ?>
								<li>
									<img src="<?php echo $bannerImage ?>">
								</li>
							<?php } ?>

						<?php 
							  	endwhile;
							  endif;
							  wp_reset_query();
						?>
						</ul>
					</div>


					<?php /* The loop */ ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<article id="post-<?php the_ID(); ?>" >
								<div class="units-container">
									<div class="units-row">
										<div class="unit-100">
											<div class="entry-content">
												<?php the_content(); ?>
												<?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentythirteen' ) . '</span>', 'after' => '</div>', 'link_before' => '<span>', 'link_after' => '</span>' ) ); ?>
											</div><!-- .entry-content -->

											<footer class="entry-meta">
												<?php edit_post_link( __( 'Edit', 'twentythirteen' ), '<span class="edit-link">', '</span>' ); ?>
											</footer><!-- .entry-meta -->
										</div>
									</div>
								</div>
							</article><!-- #post -->

							
						<?php endwhile; ?>

			<?php 

				$images = get_field('gallery');

				if( $images ): ?>
					<div class="image-gallery units-container">
					    <ul class="units-row" >
					        <?php foreach( $images as $image ): ?>
					            <li class="unit-25">
					                <a href="<?php echo $image['sizes']['large']; ?>">
					                     <img src="<?php echo $image['sizes']['gallery-thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
					                </a>
					                <p><?php echo $image['caption']; ?></p>
					            </li>
					        <?php endforeach; ?>
					    </ul>
				    </div>
			<?php endif; ?>

			

		</div><!-- #content -->
	</div><!-- #primary -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>