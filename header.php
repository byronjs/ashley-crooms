<?php
/**
 * The Header template for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<meta name="google-site-verification" content="o8eAYTaIfETPT01zK6UWLZfLjGbOyKo6YyqpzJ5KxqQ" />
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<!--[if lt IE 9]>
	<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js"></script>
	<![endif]-->
	<?php wp_head(); ?>

	<script type="text/javascript" src="http://fast.fonts.net/jsapi/89caa52c-5af6-4c94-8f2e-05e373de6260.js"></script>
	<?php if( is_front_page()) { ?>
	<script type="text/javascript">
		$(document).ready(function() {
    	$.fn.fullpage({
    		menu: '#menu',
    		easing: 'easeInOutCirc',
    		scrollingSpeed: '1000' ,
    		scrollOverflow: true

    	});
    	
});
	</script>
	<?php } ?>
</head>

<body <?php body_class(); ?>>
	<div id="page" class="hfeed site">
		<header id="masthead" class="site-header" role="banner">
			<nav class="topnav">
				<div class="row">
					<div class="col grid-12">
						<nav id="nav-main">
							<ul id="menu">
								<li class="first" data-menuanchor="firstPage">
									<a href="/#firstPage">home</a>
								</li>
								<li data-menuanchor="aboutPage" >
									<a href="/#aboutPage">about</a>
								</li>
								<li data-menuanchor="servicesPage">
									<a href="/#servicesPage">services</a>
								</li>
								<li data-menuanchor="contactPage">
									<a href="/#contactPage">contact</a>
								</li>
								<li>
									<a href="/photo-gallery">photos</a>
								</li>
							</ul>
						</nav>

						<div id="nav-trigger">
						    <span>Menu</span>
						</div>
						<nav id="nav-mobile"></nav>

					</div>
					<div class="clear"></div>
				</div>
			</nav>
		</header><!-- #masthead -->

		<div id="main" class="site-main">
